import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { Persistor, Store } from './src/Redux/store/Store'
import Route from './src/Router'

export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
            <Route />
        </PersistGate>
      </Provider>

    )
  }
}

const styles = StyleSheet.create({})
