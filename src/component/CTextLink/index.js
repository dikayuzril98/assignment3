import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const CTextLink = ({text, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={styles.instruction}>{text}</Text>
        </TouchableOpacity>
    )
}
const styles= StyleSheet.create({
    instruction:{
        textAlign:'center',
        color: 'black',
        marginBottom:5,
        marginTop:10
    },
})
export default CTextLink
