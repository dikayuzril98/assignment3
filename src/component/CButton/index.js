import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const CButton = ({text, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>{text}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles= StyleSheet.create({
    button:{
        width: 200,
        backgroundColor:'green',
        paddingHorizontal: 10,
        marginTop:10,
        alignItems:'center'
    },
    buttonText:{
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        width:100,
        color:'white'
    }
});

export default CButton;
