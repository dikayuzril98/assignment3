import AsyncStorage from '@react-native-async-storage/async-storage'
import {applyMiddleware, createStore} from 'redux'
import {persistStore} from 'redux-persist'
import persistReducer from 'redux-persist/es/persistReducer'
import thunk from 'redux-thunk'
import Reducer from '../reducer/Reducer'

const persistConfig ={
    key:'root',
    storage: AsyncStorage
}

const persistedReducer = persistReducer (persistConfig, Reducer)
const Store = createStore (persistedReducer, applyMiddleware(thunk))
const Persistor = persistStore (Store)

export {Store, Persistor}