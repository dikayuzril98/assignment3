const inputform = (value) => {
    return (dispatch) => {
        dispatch({type: 'INPUT-FORM', payload: value})
    }
}

const RegisterUser = (users) => {
    return (dispatch) => {
        dispatch ({type: 'REGISTER-USER', payload: users})
    }
}

export {inputform, RegisterUser}