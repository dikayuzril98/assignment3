const initialState = {
    name: '',
    data: [],
    inputform: {}
}

const Reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'INPUT-FORM':
            return {
                ...state,
                inputform: action.payload
            }
        case 'REGISTER-USER':
            return {
                ...state,
                data: [...state.data, action.payload]
            }
        default:
            return state
    }
}

export default Reducer