import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Splash from '../Screens/Splash'
import Login from '../Screens/Login'
import Register from '../Screens/Register'
import Home from '../Screens/Home'

const Stack = createStackNavigator()

const StackNavigator = () =>{
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name='Splash'
                component={Splash}
                options={{headerShown:false}}/>
            <Stack.Screen 
                name='Login'
                component={Login}
                options={{headerShown:false}}/>
            <Stack.Screen 
                name='Register'
                component={Register}
                options={{headerShown:false}}/>
            <Stack.Screen 
                name='Home'
                component={Home}
                options={{headerShown:false}}/>
        </Stack.Navigator>
    )
}
const Route =() => {
    return(
        <NavigationContainer>
            <StackNavigator />
        </NavigationContainer>
    )
}
export default Route