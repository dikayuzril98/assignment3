import React, { Component } from 'react'
import { Text, StyleSheet, View, Image} from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { RegistUser } from '../Redux/action/Action'
import { connect } from 'react-redux'
import CButton from '../component/CButton'
import CTextLink from '../component/CTextLink'
import InputText from '../component/CTextInput/InputText'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
        }
    }

    validate() {
        const { username, password } = this.state
        const user = this.props.data.filter((item) => item.name === username)
        console.log('users', user)
        try {
            if (user[0].name == username) {
                if (user[0].password === password) {
                    console.log('Login Berhasil')
                    { this.props.navigation.replace('Home') }
                } else {
                    {alert('password salah')
                }}
            } else {
                {alert('username salah')
            }}
        } catch (error) {
            alert('isi data')
        }
    }

    render() {
        return (
            <View style={style.container}>
                <Image
                    source={require('../assets/images/MHD.png')}
                    style={{ height: 100, width: 100, margin: 10 }} />
                <Text style={style.text}>Salt Academy</Text>
                <Text style={style.text}>Please login with a registered account</Text>

                <TextInput
                    style={style.textInput}
                    placeholder='Username'
                    value={this.state.username}
                    onChangeText={(i) => {
                        this.setState({ username: i })
                    }} />
                <TextInput
                    secureTextEntry
                    style={style.textInput}
                    placeholder='Password'
                    value={this.state.password}
                    onChangeText={(i) => {
                        this.setState({ password: i })
                    }} />
 
                <CButton text='Login' onPress={() => this.validate()}/>
                <CTextLink text='Do not have an account? Sign Up' onPress={() => this.props.navigation.navigate('Register')}/>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        backgroundColor: 'lavender'
    },
    textInput: {
        height: 40,
        borderWidth: 1,
        color: 'black',
        paddingHorizontal: 10,
        marginBottom: 10,
        width: 350,
        backgroundColor: '#FDF5E6',
        borderRadius: 10
    },
    buttonStyle: {
        width: 200,
        backgroundColor: 'green',
        paddingHorizontal: 10,
        marginTop: 10,
        alignItems: 'center'
    },
    instruction: {
        textAlign: 'center',
        color: 'black',
        marginBottom: 5,
        marginTop: 10
    },
    txtLogin: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        width: 100,
        color: 'white'
    },
    text: {
        fontWeight: 'bold',
        fontSize: 13,
        marginBottom: 5
    }
})

const mapStatetoProps = (state) => {
    return {
        data: state.data
    }
}
const mapDispatchtoProps = (dispatch) => {
    return {
        RegisterUser: (value) => dispatch(RegistUser(value))
    }
}
export default connect(mapStatetoProps, mapDispatchtoProps)(Login)