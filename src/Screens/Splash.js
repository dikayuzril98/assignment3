import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'


class Splash extends Component {
    componentDidMount(){
        setTimeout(() =>{
            this.props.navigation.replace('Login')
        }, 2000)
    }
    render() {
        return (
            <View style={style.container}>
                <Image source={require ('../assets/images/MHD.png')} style={style.image}/>
                <Text style={style.text}>Salt Academy</Text>
            </View>
        )
    }
}
export default Splash
const style = StyleSheet.create({
    container: {
        backgroundColor: 'mediumslateblue',
        flex: 1,
        alignItems: 'center',
        justifyContent:'center'
    },
    image: {
        height: 150,
        width: 150
    },
    text: {
        color: 'white',
        marginTop: 10,
        fontSize: 24,
        fontWeight:'bold',
        color:'black'
    }
})
