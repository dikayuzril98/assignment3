import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { TextInput} from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import { inputform, RegisterUser } from '../Redux/action/Action'
import CButton from '../component/CButton'
import CTextLink from '../component/CTextLink'

class Register extends Component {
    collectData = (inputType, value) => {
        this.setState({
            [inputType]: value
        })
        this.props.input(this.state)
    }

    handleButtonRegister = () => {
        const {name, password} = this.props.inputform
        this.props.RegisterUser({name, password})
        this.props.navigation.navigate('Login')
    }
    render() {
        return (
            <View style = {style.container}>
                <Image 
                    source={require('../assets/images/MHD.png')}
                    style={{height:100, width:100, margin:10}}/>
                <Text style ={style.text}> Salt Academy </Text>
                <Text style ={style.text}> Please Register with valid data</Text>
                <TextInput 
                    style={style.textInput}
                    placeholder='Name'
                    onChangeText={(i) => this.collectData('name',i)}/>
                <TextInput 
                    style={style.textInput}
                    placeholder='Email'
                    onChangeText={(i) => this.collectData('email',i)}/>
                <TextInput 
                    style={style.textInput}
                    placeholder='Phone Number'
                    keyboardType='number-pad'
                    onChangeText={(i) => this.collectData('phone',i)}/>
                <TextInput 
                    secureTextEntry
                    style={style.textInput}
                    placeholder='Password'
                    onChangeText={(i) => this.collectData('password',i)}/>
                <TextInput 
                    secureTextEntry
                    style={style.textInput}
                    placeholder='Confirm Password'
                    onChangeText={(i) => this.collectData('confirmPass',i)}/>
                
                <CButton onPress={() => this.handleButtonRegister()} text='Register' />
                <CTextLink onPress = {() => this.props.navigation.navigate('Login')} text='Click here if you already have an account'/>
                
            </View>
        )
    }
}

const style = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        color:'white',
        backgroundColor:'lavender'
    },
    textInput: {
        height:40,
        borderWidth:1,
        color:'black',
        paddingHorizontal: 10,
        marginBottom: 10,
        width:350,
        backgroundColor:'#FDF5E6',
        borderRadius:10
    },
    txtSignUp: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        width:100,
        color:'white'
    },
    buttonStyle: {
        width: 200,
        backgroundColor:'green',
        paddingHorizontal: 10,
        marginTop:10,
        alignItems:'center'
    },
    instruction:{
        textAlign:'center',
        color: 'white',
        marginBottom:5,
        marginTop:10
    },
    text: {
        fontWeight:'bold',
        fontSize:13,
        marginBottom:5
    }
})

const mapStatetoProps = (state) => {
    return {
        name: state.name,
        inputform: state.inputform,
        data: state.data
    }
}
const mapDispatchtoProps = (dispatch) => {
    return {
        input: (value) => dispatch(inputform(value)),
        RegisterUser: (value) => dispatch(RegisterUser(value))
    }
}
export default connect(mapStatetoProps, mapDispatchtoProps)(Register)